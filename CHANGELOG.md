# [1.1.0](https://github.com/roalcantara/cockpit/compare/v1.0.0...v1.1.0) (2022-12-04)


### Bug Fixes

* **ci:** Fix deployment workflow ([912cdfe](https://github.com/roalcantara/cockpit/commit/912cdfe5032846b7189312d26d55b61852441899))


### Features

* **shells:** Add ZSH as default shell ([64f88ba](https://github.com/roalcantara/cockpit/commit/64f88ba0ca5c9ae141dc88743997a54e31672ed9))

# 1.0.0 (2022-12-04)


### Bug Fixes

* **ci:** Fix gpg references ([42ed701](https://github.com/roalcantara/cockpit/commit/42ed7016901d1a709f4101da13f011cf9fa966cf))
* **ci:** Fix release workflow ([d29a070](https://github.com/roalcantara/cockpit/commit/d29a070bc19e5cf01e1cf8afaad5b60a76dfbcc8))
* **ci:** Fix semantic release ([f609cbd](https://github.com/roalcantara/cockpit/commit/f609cbd63170c766dae8bce96b27b0c110a86886))
* **ci:** Semantic Release ([00fef23](https://github.com/roalcantara/cockpit/commit/00fef23dcfc12a1e54085af1814d07de96700ca6))
* **ci:** Set GPG Settings ([48a7e2e](https://github.com/roalcantara/cockpit/commit/48a7e2ebb1e81520dbd8f1e8b9a27746b3c4ce34))


### Features

* **git:** Add config and dependencies ([ed77eb3](https://github.com/roalcantara/cockpit/commit/ed77eb32602850da310974a01c456ed02d29176f))
